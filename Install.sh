#!/bin/bash

#package_name=$1

pkg=$1
status="$(dpkg-query -W --showformat='${db:Status-Status}' "$pkg" 2>&1)"
if [ ! $? = 0 ] || [ ! "$status" = installed ]; then
  sudo apt install $pkg
else
  echo "[Message]:${pkg} is already in your system"
fi
